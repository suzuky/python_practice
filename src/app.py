from flask import Flask, jsonify

from controller import members


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

# http://localhost:5000/members にアクセスするとcontroller/members.pyが呼び出されている
app.register_blueprint(members.app)


@ app.route('/')
def root():
    json = {
        "hoge": 'fuga'
    }
    return jsonify(json), 200


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)
