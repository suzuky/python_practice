import os

from dotenv import load_dotenv
from sqlalchemy import create_engine

env_path = '../.env'
load_dotenv(dotenv_path=env_path)


def connect_to_db():
    """ DBとつながるやつ """
    DATABASE = 'mysql://%s:%s@%s:%s/%s?charset=utf8' % (
        os.getenv("DATABASE_ID"),
        os.getenv("DATABASE_PW"),
        "192.168.1.39",
        "3306",
        os.getenv("DATABASE_DB")
    )
    ENGINE = create_engine(
        DATABASE, encoding="utf-8", echo=False)

    return ENGINE
