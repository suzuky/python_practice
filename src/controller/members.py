from flask import Blueprint, jsonify

from model import engine, members


# Blueprintのオブジェクトを生成する
app = Blueprint('controller', __name__)


engine = engine.connect_to_db()
result = members.fetch_members_result(engine)
# print(engine)
# print(result)

json = []
data = {}
for row in result:
    for column, value in row.items():
        data = {**data, **{column: value}}
    json.append(data)


@app.route('/members')
def members():
    return jsonify(json), 200
